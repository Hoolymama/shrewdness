#include <maya/MPxNode.h>
#include <maya/MFnArrayAttrsData.h>
#include <maya/MString.h>

class instanceOverride: public MPxNode {
	public:
virtual	void		postConstructor();
		instanceOverride (); virtual ~instanceOverride ();
		virtual MStatus compute (const MPlug& plug, MDataBlock& data);
		static void *creator (); 
		static MStatus initialize (); 
		static MTypeId id;
	private:
		static MObject aInput;
		static MObject aDataName;	   
		static MObject aParticleId;	   
		static MObject aOverrideList;
		static MObject aValue;
		static MObject aOutput;

void  invalidMessage(
	MFnArrayAttrsData &fn, MString name) ;

};
