#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnTypedAttribute.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MVectorArray.h> 
#include <maya/MDoubleArray.h> 
#include <maya/MGlobal.h> 
#include <maya/MFnDependencyNode.h> 

#include "errorMacros.h"
#include "instanceOverride.h"
#include "jMayaIds.h"


MTypeId instanceOverride::id(k_instanceOverride);


MObject instanceOverride::aInput;
MObject instanceOverride::aDataName;	   
MObject instanceOverride::aParticleId;	   
MObject instanceOverride::aOverrideList;
MObject instanceOverride::aValue;
MObject instanceOverride::aOutput;

void * instanceOverride::creator () {
	return new instanceOverride;
}

/// Post constructor
void
	instanceOverride::postConstructor()
{
	MPxNode::postConstructor();

	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus instanceOverride::initialize () {

	MStatus st;


	MFnTypedAttribute tAttr;
	MFnNumericAttribute nAttr;



	aInput = tAttr.create("input", "in",MFnData::kDynArrayAttrs);
	tAttr.setWritable(true);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	addAttribute( aInput ); 

	aDataName = tAttr.create("dataName", "dn",MFnData::kString);
	tAttr.setStorable(true);
	addAttribute( aDataName ); 

	aParticleId= tAttr.create("particleId", "pid",MFnData::kDoubleArray);
	tAttr.setWritable(true);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	addAttribute( aParticleId ); 

	aOverrideList = tAttr.create( "overrideList", "ols", MFnData::kString );
	tAttr.setStorable(true);
	addAttribute( aOverrideList );

	aValue = nAttr.create( "overrideValue", "ov",MFnNumericData::k3Double);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault(0.0,0.0,0.0);
	addAttribute( aValue ); 

	aOutput = tAttr.create ("output", "out",MFnData::kDynArrayAttrs);
	tAttr.setStorable (false);
	tAttr.setWritable (false);
	tAttr.setReadable (true);
	addAttribute (aOutput);


	attributeAffects (aInput, aOutput);
	attributeAffects (aDataName, aOutput);
	attributeAffects (aParticleId, aOutput);
	attributeAffects (aOverrideList, aOutput);
	attributeAffects (aValue, aOutput);




	return MS::kSuccess;
}

instanceOverride::instanceOverride () {}
instanceOverride::~instanceOverride () {}


void instanceOverride::invalidMessage(MFnArrayAttrsData &fn, MString name) {


	MGlobal::displayWarning("instanceOverride");
	MString msg("instanceOverride ") ;

	msg += MFnDependencyNode(thisMObject()).name();
	msg += 	" invalid data name requested or no IDs given. FYI: Options are:";	
	MGlobal::displayWarning(msg);
	MStringArray names = fn.list();
	for (unsigned i = 0; i < names.length(); ++i)
	{
		MFnArrayAttrsData::Type attrType = MFnArrayAttrsData::kInvalid;
		fn.checkArrayExist(names[i], attrType);
		if (attrType == MFnArrayAttrsData::kVectorArray) {
			MGlobal::displayInfo(names[i] + " (VectorArray)");
			continue;
		}
		if  (attrType == MFnArrayAttrsData::kDoubleArray) {
			MGlobal::displayInfo(names[i] + " (DoubleArray)");
			continue;
		}
		if (attrType == MFnArrayAttrsData::kIntArray) {
			MGlobal::displayInfo(names[i] + " (IntArray)");
			continue;
		}
	}
}

MStatus instanceOverride::compute(const MPlug& plug, MDataBlock& data) {

	if (!(plug == aOutput)) 	return MS::kUnknownParameter;			

	MStatus st;


	MFnArrayAttrsData fnOut;
	MObject dOut = fnOut.create ( &st );er;

	MDataHandle hOut = data.outputValue(aOutput);

	MDataHandle hIn = data.inputValue(aInput);
	MObject dIn = hIn.data();
	MFnArrayAttrsData fnA(dIn, &st); er;

	MString idList =  data.inputValue(aOverrideList ).asString();
	MString dataName =  data.inputValue(aDataName ).asString();

	MDataHandle hParticleIds = data.inputValue(aParticleId);
	MObject dParticleIds = hParticleIds.data();
	MDoubleArray particleIds = MFnDoubleArrayData(dParticleIds).array();
	unsigned pl = particleIds.length();

	MVector value = data.inputValue(aValue).asVector();

	MFnArrayAttrsData::Type attrType = MFnArrayAttrsData::kInvalid;
	bool nameValid = fnA.checkArrayExist (dataName, attrType);

	MStringArray stra;
	idList.split(' ', stra);	
	unsigned nList   = stra.length();
	MIntArray ids;
	if (nList > 0){
		for (unsigned i = 0; i < nList; ++i)
		{
			ids.append(stra[i].asInt());
		}
	} 

	if ( 
		(!nameValid) || 
		(attrType == MFnArrayAttrsData::kStringArray) ||
		(attrType == MFnArrayAttrsData::kInvalid) ||
		(ids.length() == 0)
		)
	{
		invalidMessage(fnA, dataName);
		hOut.set(dIn);
	} else {

		// make a new copy of all the data.
		// seems a bit wasteful - but what the hell
		// cerr << "copying arrays to out" << endl;
		//MObject dOut = hOut.data();
		//MFnArrayAttrsData fnOut(dOut);
		MStringArray names = fnA.list();

		// MVectorArray va;
		// MVectorArray da;
		// MVectorArray ia;
		// MVectorArray sa;

		for (unsigned i = 0; i < names.length(); ++i)
		{

			MFnArrayAttrsData::Type at = MFnArrayAttrsData::kInvalid;

			fnA.checkArrayExist(names[i], at);
			if (at == MFnArrayAttrsData::kVectorArray) {
				// cerr << "type = kVectorArray" << endl;
				MVectorArray va = fnOut.vectorArray(names[i]);
				va.copy(fnA.getVectorData(names[i]));
			} else if  (at == MFnArrayAttrsData::kDoubleArray) {
				// cerr << "type = kDoubleArray" << endl;
				MDoubleArray da = fnOut.doubleArray(names[i]);
				da.copy(fnA.getDoubleData(names[i]));
			} else if (at == MFnArrayAttrsData::kIntArray) {
				// cerr << "type = kIntArray" << endl;
				MIntArray ia = fnOut.intArray(names[i]);
				ia.copy(fnA.getIntData(names[i]));
			} else if (at == MFnArrayAttrsData::kStringArray) {
				// cerr << "type = kStringArray" << endl;
				MStringArray sa = fnOut.stringArray(names[i]);
				sa = fnA.getStringData(names[i]); 
			}
		}

		if (attrType == MFnArrayAttrsData::kIntArray) {
			MIntArray ta = fnOut.intArray(dataName);
			if (ta.length() != pl) {
				// cerr << "kIntArray" << dataName << endl;
				// cerr << "ta.length()" << ta.length() << endl;
				// cerr << "pl" << pl << endl;
				MGlobal::displayError("Data length doesn't match length of particle IDs in instanceOverride");
			} else {
				for (unsigned i = 0; i < pl; ++i)
				{
					for (unsigned j = 0; j < nList; ++j)
					{
						if (particleIds[i] == ids[j]) {
							ta[i] = int(value.x);
							break;
						}
					}
				}
			}
		} else if (attrType == MFnArrayAttrsData::kDoubleArray) {
			MDoubleArray ta = fnOut.doubleArray(dataName);
			if (ta.length() != pl) {
				// cerr << "kDoubleArray" << dataName << endl;
				// cerr << "ta.length()" << ta.length() << endl;
				// cerr << "pl" << pl << endl;
				MGlobal::displayError("Data length doesn't match length of particle IDs in instanceOverride");
			} else {
				for (unsigned i = 0; i < pl; ++i)
				{
					for (unsigned j = 0; j < nList; ++j)
					{
						if (particleIds[i] == ids[j]) {
							ta[i] = value.x;
							break;
						}
					}
				}
			}
		} else if (attrType == MFnArrayAttrsData::kVectorArray) {
			MVectorArray ta = fnOut.vectorArray(dataName);
			if (ta.length() != pl) {
				// cerr << "kVectorArray" << dataName << endl;
				// cerr << "ta.length()" << ta.length() << endl;
				// cerr << "pl" << pl << endl;
				MGlobal::displayError("Data length doesn't match length of particle IDs in instanceOverride");
			} else {
				for (unsigned i = 0; i < pl; ++i)
				{
					for (unsigned j = 0; j < nList; ++j)
					{
						if (particleIds[i] == ids[j]) {
							ta[i] = value;
							break;
						}
					}
				}
			}
		} 
		hOut.set(dOut); 
	}
	data.setClean(plug);
	return MS::kSuccess;			
}
