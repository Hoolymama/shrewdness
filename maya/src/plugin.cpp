#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>

#include "errorMacros.h" 

#include "instanceOverride.h" 


MStatus initializePlugin( MObject obj)
{

	MStatus st;
	
	MString method("initializePlugin");

	MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);

	st = plugin.registerNode( "instanceOverride", instanceOverride::id, instanceOverride::creator, instanceOverride::initialize); ert;
	
	MGlobal::executePythonCommand("import shrewdness;shrewdness.load()");

	return st;

}

MStatus uninitializePlugin( MObject obj)
{
	MStatus st;

	MString method("uninitializePlugin");

	MFnPlugin plugin( obj );

	st = plugin.deregisterNode( instanceOverride::id );ert;


	return st;
}

