import pymel.core as pm
import random
def do_rand_set():
    name = pm.textFieldGrp('rs_name_tf',query=True, text=True)
    numSets = pm.intFieldGrp('rs_num_sets_if', query=True, value1=True)
    sets = []
    sel = pm.ls(selection=True)
    for i in range(numSets):
        pm.select(cl=True)
        sets.append(pm.sets(name="%s_%d" % (name, i)))
    [pm.sets(sets[random.randrange(0, numSets)], add=o) for o in sel]
    
def rand_set_win(): 
    win = pm.window(w=400)
    pm.columnLayout()
    pm.textFieldGrp('rs_name_tf', label='Name', text='my_set')
    pm.intFieldGrp('rs_num_sets_if', label='Number of Sets', value1=3)
    pm.button("GO", w=400, command=pm.Callback(do_rand_set))
    pm.showWindow( win )
