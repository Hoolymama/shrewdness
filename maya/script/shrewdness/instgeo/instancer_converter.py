import os
import pymel.core as pm

class InstancerConverter(object):

    def __init__(self, particle_system, **kwargs):

        if not pm.system.pluginInfo('Flock', q=True, loaded=True):
            pm.system.loadPlugin('Flock')

        self._do_instances = kwargs.get('instances', True)
        self._identifier = kwargs.get('identifier', 'new_object')
        self._do_euler = kwargs.get('euler', True)
        self._ai_standins = kwargs.get('ai_standins', True)
        self._bake = kwargs.get('bake', 2)


        self._psys = particle_system
        inst = pm.listConnections(self._psys.attr('instanceData'), d=True, s=False)
        if not inst:
            raise ValueError, 'No Instancer'
        self._inst = inst[0]

        self._sourceObjects = pm.listConnections(self._inst.attr('inputHierarchy'), s=True, d=False)
        if not self._sourceObjects:
            raise ValueError, 'No Source Objects'

        # Change units so that arrayToMulti rotation node
        # deals only with radians. No need for unitConversion
        # nodes
        au = pm.currentUnit(query=True, angle=True)
        lu = pm.currentUnit(query=True, linear=True)
        pm.currentUnit(angle='rad')
        pm.currentUnit(linear='cm')

        self._start =  pm.playbackOptions(query=True, min=True)
        self._end =  pm.playbackOptions(query=True, max=True)


        pm.progressWindow(title='Convert Instancer Progress',
                          isInterruptable=True, width=500)

        self.go()

        pm.progressWindow(endProgress=True)
        pm.currentUnit(angle=au)
        pm.currentUnit(linear=lu)

    def go(self):

        # need object Ids and particleIds to make sure we get the
        # correct source instance object, and also for naming.
        objectIds = [ int(x) for x in self._psys.attr('instancerIndex').get()]
        particleIds = [ int(x) for x in self._psys.attr('particleId').get()]
        pl = len(particleIds)
        pm.progressWindow(edit=True, status='Duplicating Objects', max=pl)

        if self._ai_standins:
            self._standinify()
           
        self._create_new_instances(objectIds, particleIds)

        pm.progressWindow(edit=True, status='Adding translate A2M')
        self._connect_a2m('worldPosition', 'translate')

        pm.progressWindow(edit=True, status='Adding rotate A2M')
        self._connect_a2m('rotation', 'rotate')

        pm.progressWindow(edit=True, status='Adding scale A2M')
        self._connect_a2m('scale', 'scale')

        # euler filter fix
        if not self._bake == 'None':
            pm.progressWindow(edit=True, status='Baking')
            self._do_bake()

        self._cleanup()

    def _cleanup(self):

        pm.delete(self._sourceObjects)
        pm.delete(self._inst)
        if self._bake == 'All':
            pm.delete(self._psys)


    def _do_bake(self):
        atts = ('rx','ry','rz')
        if self._bake == 'All':
            atts = ('tx','ty','tz','rx','ry','rz','sx','sy','sz')
        pm.bakeResults( self._newObjects, at=atts, t=(self._start-1, self._end+1), simulation=True )
        i=0
        pm.progressWindow(edit=True, status='Applying Euler Filter')
        for obj in self._newObjects:
            if self._do_euler:
                curves = pm.listConnections(obj, s=True, d=False, type='animCurveTA')
                pm.filterCurve(curves)
            i+=1
            pm.progressWindow(edit=True, progress=i)
            self._check_esc()         

    def _create_new_instances(self, objectIds, particleIds):
        num = len(particleIds)
        self._newObjects = []
        for i in range(num):
            particleId = particleIds[i]
            objectId = objectIds[i]
            newObject = pm.duplicate(self._sourceObjects[objectId], 
                    rr=True, instanceLeaf=self._do_instances)[0]
            newObject.rename('%s_%d_%d' % (self._identifier, objectId, particleId))
            self._newObjects.append(newObject)
            pm.progressWindow(edit=True, progress=i)
            self._check_esc()

    def _check_esc(self):
        if pm.progressWindow(query=True, isCancelled=True):
            pm.progressWindow(endProgress=True)
            raise KeyboardInterrupt

    def _connect_a2m(self, src_attr, dest_attr):

        a2m = pm.createNode('arrayToMulti')
        a2mOut = 'outputsD'
        if self._psys.attr(src_attr).type() == 'vectorArray':
            a2mOut = 'outputsV'

        self._psys.attr(src_attr) >> a2m.attr('input')
        if isinstance(dest_attr, str):
            dest_attr = [dest_attr]

        i = 0
        for n in self._newObjects:
            n = pm.PyNode(n)
            for d in dest_attr:
                a2m.attr(a2mOut)[i] >> n.attr(d)
            i += 1
            pm.progressWindow(edit=True, progress=i)
            self._check_esc()

        a2m.rename('a2m_' + self._identifier + '_' + src_attr + '_to_' + dest_attr[0])
        return a2m

    def _standinify(self):
        mtoa = pm.pluginInfo('mtoa', query=True, loaded=True)
        if not mtoa:
            pm.loadPlugin('mtoa')
        mtoa = pm.pluginInfo('mtoa', query=True, loaded=True)
        if not mtoa:
            print 'Skipping standin generation as mtoa not available'
            return



        # find somewhere to put the ass files
        ws = pm.workspace(query=True, rd=True)
        cache_dir = os.path.join(ws , 'cache', self._identifier)
        if not os.path.exists(cache_dir):
            os.makedirs(cache_dir)

        pm.createNode("objectSet", name="ArnoldStandInDefaultLightSet", shared=True)

        for tf in self._sourceObjects:
            shapes = pm.ls(tf, dag=True, shapes=True)
            for shape in shapes:
                legalName =  self._sanitize(shape)       

                #get the short name in order to rename the standin
                shortName = shape.split('|')[-1]

                # to ensure correct local transform, put the shape
                # under an identity transform
                # (and grab its bounding box)
                grp = pm.group(em=True)
                pm.parent(shape, grp, r=True, s=True)
                bb = grp.attr('boundingBox').get()
                fn = os.path.join(cache_dir, '%s.ass' % legalName)
                pm.select(grp)
                pm.arnoldExportAss(filename=fn, selected=True, mask=255, lightLinks=0, shadowLinks=0)
                # we can now delete the shape (which will be in the temp grp)
                pm.delete(grp)

                # create the standin node and set its dso path, BB, render flags etc.
                standin = pm.createNode('aiStandIn', parent=tf, name=shortName)
                pm.sets('ArnoldStandInDefaultLightSet', add=standin)

                standin.MinBoundingBox0.set(bb[0][0])
                standin.MinBoundingBox0.set(bb[0][1])
                standin.MinBoundingBox0.set(bb[0][2])
                standin.MaxBoundingBox0.set(bb[1][0])
                standin.MaxBoundingBox0.set(bb[1][1])
                standin.MaxBoundingBox0.set(bb[1][2])

                standin.dso.set(fn)
                standin.visibleInReflections.set(1)
                standin.visibleInRefractions.set(1)
                standin.mode.set(2)
                print 'Standin saved: %s ' % fn

    @staticmethod
    def _sanitize(name):
        for char in [':', '-', '.', '|' ]:
            name  = name.replace(char, '_') 
        return name

def do_inst_convert():
    name =  pm.textFieldGrp('ic_name_tf', query=True, text=True)

    inst = pm.checkBoxGrp('ic_do_inst_cb', query=True, value1=True)

    bake = pm.optionMenuGrp('ic_bake_om', query=True, value=True)
    euler = pm.checkBoxGrp('ic_do_euler_cb', query=True, value1=True)
    standins = pm.checkBoxGrp('ic_do_ai_standins', query=True, value1=True)

    if bake=='None':
        euler = False
    
    psys = pm.ls(sl=True, dag=True, leaf=True, type='particle')

    if psys:
        psys = psys[0]
        InstancerConverter(
                psys, euler=euler, bake=bake, ai_standins=standins, 
                instances=inst, identifier=name)
    else:
        print 'No Particles Picked'


def inst_convert_win(): 
    win = pm.window(w=400)
    pm.columnLayout()
   
    pm.textFieldGrp('ic_name_tf', label='Name', text='my_stuff')

    pm.checkBoxGrp('ic_do_inst_cb', label='Make Instances', value1=True)

    pm.optionMenuGrp('ic_bake_om',label='Bake')
    [pm.menuItem( label=x ) for x in ['None', 'Rotation', 'All', ]]
    pm.optionMenuGrp('ic_bake_om', edit=True, value='All')

    pm.checkBoxGrp('ic_do_euler_cb', label='Euler Filter', value1=True)

    pm.checkBoxGrp('ic_do_ai_standins', label='Make AI Standins', value1=True)

    pm.button("GO", w=400, command=pm.Callback(do_inst_convert))
    pm.showWindow( win )
